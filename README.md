Is this AOC thing hard?
===

This is my TypeScript playground for [Advent of Code 2021](https://adventofcode.com/).

## Assumptions

- Each day's puzzle will have an input consisting of one or more lines of plain text.
- Each day's puzzle will have two "parts" requiring separate implementations, but using the same input.
- Puzzle solutions will be a single line of text.
- Each puzzle description will provide at least one example input/output.

## Structure

    data/inputs/:year/:day/input    # input data, obviously
    src/
        template.ts                     # a template file to copy into <day number>.ts, e.g. 3.ts
        util/                           # helpers and stuff
        2021/
            1.ts                        # day 1 of 2021
            ...
        2020/
            ...
    build/                              # esbuild config
    dist/:year/:day.js                  # scripts for given days and years

## How to use

Since I'll forget the workflow for the next year, here:

- `npm run get-input:today`: download input data for a new day to [data folder](data/inputs/) as `data/inputs/_YEAR_/_DAY_/input`
  - make sure you had the AOC session cookie env var, look at [.env](.env.template) template for that.
- copy [`template.ts`](src/template.ts) to `<day number>.ts` in [src/YEAR](src/2021)
- `npm start`: run esbuild in watch mode, any changes to src/<years>/<day number>.ts get recompiled
- `npm run run:today`: run the today's script in watch mode
- `npm run run:today -- 3 2020`: run day 3 of 2020 in watch mode


I may or may not improve this process.

## Type checks.

I got IntelliJ that helps me do typechecks. I don't care right now to setting up an extra server.
