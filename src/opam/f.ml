let input = {|
  199
  200
  208
  210
  200
  207
  240
  269
  260
  263
|}


let get_lines i = i |> String.split_on_char '\n';;
let l = get_lines input;;
let res = l |> List.filter (fun s -> s <> "") |> List.map int_of_string;;

let cnt_it depths =
  match depths with
    | [] -> failwith "List empty"
    | first::rest -> first

cnt_it res

