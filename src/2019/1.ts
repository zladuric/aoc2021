import { execute } from '../util';

const getFuelNeeded = (mass: number): number => Math.floor(mass / 3) - 2;

const part1 = (input: string[]): number => {
  return input
    .map(s => parseInt(s, 10))
    .map(mass => getFuelNeeded(mass))
    .reduce((acc, cur) => acc + cur, 0);
};
const getMassWithFuel = (mass: number): number => {
  let totalFuel = getFuelNeeded(mass);
  let newFuel = getFuelNeeded(totalFuel);
  while (newFuel > 0) {
    totalFuel = totalFuel + newFuel;
    newFuel = getFuelNeeded(newFuel);
  }
  return totalFuel;
};
const part2 = (input: string[]): number => {
  return input
    .map(s => parseInt(s, 10))
    .map(mass => getMassWithFuel(mass))
    .reduce((acc, cur) => acc + cur, 0);
};


execute(__filename, part1, part2);
