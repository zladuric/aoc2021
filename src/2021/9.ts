import { execute } from '../util';

const part1 = (input: string[]): number => {
  return input.length;
};

const part2 = (input: string[]): number => {
  return input.length;
};


execute(__filename, part1, part2);
