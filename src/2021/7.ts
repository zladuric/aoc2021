import { execute } from '../util';

const testInput = `16,1,2,0,4,2,7,1,2,14`.split('\n');
interface Bucket {
  [key: number]: number;
}

const parseInput = (i: string[]): number[] => i[0]
  .split(',')
  .map(n => parseInt(n, 10));

const sortPositions = (input: number[]): Bucket => input
  .reduce((b, i) => ({
    ...b,
    [i]: b[i] ? b[i] + 1 : 1,
  }), {});

const calculateCost = (b: Bucket, pos: number): number => Object.entries(b).reduce((cost, entry) => cost + Math.abs(pos - entry[0]) * entry[1], 0)

const getAllPos = (d: number[]): number[] => {
  console.log('D:', d);
  const min = Math.min(...d);
  const max = Math.max(...d);
  console.log('Minmax:', min, max);
  const positions = Array.from(Array(max - min).keys())
    .map(val => val + min)
  return positions;
}

const calculateCosts = (b: Bucket, positions: number[], calculateCost: (b: Bucket, n: number) => number): number => {
  const res = positions
    .map(position => ({
      position,
      cost: calculateCost(b, position),
    }))
    .sort((a, b) => a.cost - b.cost);
  return res[0].cost;
}

const part1 = (input: string[]): number => {
  const data = parseInput(input);
  const b = sortPositions(data);
  // console.log(JSON.stringify(b, null, 2));
  const positions = getAllPos(data);
  const cost = calculateCosts(b, positions, calculateCost);
  return cost;
};
const calculateExpensiveCost = (b: Bucket, pos: number): number => Object.entries(b)
  .reduce((cost, entry) => {
    const cur = parseInt(entry[0], 10);
    const dist = Math.abs(pos - cur );
    const extraCost = (dist * (dist + 1))/2;
    console.log(`From ${cur} to ${pos} is ${dist}, costing ${extraCost} per unit`);
    // console.log('D, C:', dist, extraCost, `to pos: ${pos}`, cur);
    return cost + extraCost * entry[1];
  }, 0);


const part2 = (input: string[]): number => {
  const data = parseInput(input);
  const b = sortPositions(data);
  // console.log(JSON.stringify(b, null, 2));
  const positions = getAllPos(data);
  const cost = calculateCosts(b, positions, calculateExpensiveCost);
  // console.log(calculateExpensiveCost(b, 5))
  return cost;
};


// execute(__filename, part1, part2);

const p1 = part1(testInput);
const p2 = part2(testInput)
console.log(`P1: ${p1}, ${p1 === 37 ? 'correct' : 'incorrect' }`);
console.log(`P2: ${p2}, ${p2 === 168 ? 'correct' : 'incorrect' }`);
execute(__filename, part1, part2)


class Test {
  public static test1(): void {
    const input = `0,3,0,1,-3`.split('\n');
    const result = part1(input);
    console.log(`Result: ${result}`);
    if (result !== 5) {
      throw new Error('Test failed');
    }
  }
}
