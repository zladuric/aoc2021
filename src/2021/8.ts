import { execute } from '../util';

const testInput = `be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce`.split('\n');

const splitLine = (l: string): string => l.split(' | ');


const getOutputs = (i: string[]): string[] => i
  .filter(l => l)
  .map(line => line.split(' | ')[1])
//   .filter(l => !l.includes('|'))

const uniqueCounts = [2, 3, 4, 7] 

const countDigits = (line: string): number => line
  .split(' ')
  .map(s => s.length)
  .filter(l => uniqueCounts.includes(l)).length

const part1 = (input: string[]): number => {
  const outputs = getOutputs(input);
  /*
  console.log('Outputs:', outputs);
  console.log(outputs.map(countDigits))
  */
  return outputs.reduce((sum, line) => sum + countDigits(line), 0);
};

const cntToDig = {
  2: [1],
  3: [7],
  4: [4],
  5: [2, 3, 5],
  6: [0, 6, 9],
  7: [8]
}

interface SegMap {
  seg: string;
  pos: number[];
  digit: number
}

const getKnownDigit = (thing: SegMap): SegMap => {
  if (cntToDig[thing.seg.length].length === 1) {
    return { ...thing, digit: cntToDig[thing.seg.length][0] }
  } else {
    return thing;
  }
}

const findMiddles = (seven, four) => four.split('').filter(c => !seven.includes(c)).join('');
const findTop = (one, seven) => seven.split('').find(c => !one.includes(c));

const segHasOtherSeg = (outer, inner): boolean => {
  const out = outer.seg.split('');
  const inn = inner.seg.split('');
  return inn.every(c => out.includes(c));
}

const getPossibleDigits = (seg: string) => cntToDig[seg.length];
const segMatch = /([a-z]+)/g;

const minus = (outer, inner): string => {
  const { seg } = outer;
  const { seg: inn } = inner;
  return inn.split('').find(c => !seg.includes(c));
}

const processLine = (line: string) => {
  const allSegs = line.split('|')[0].match(segMatch)
  const possibles = allSegs.map(seg => ({seg, pos: getPossibleDigits(seg) }));

  const digits = possibles.map((possible, idx) => getKnownDigit(possible, idx, possibles, allSegs));
  console.log('Before:', digits)

  const one = digits.find(d => d.digit === 1);
  const four = digits.find(d => d.digit === 4);
  const seven = digits.find(d => d.digit === 7);
  const eight = digits.find(d => d.digit === 8);

  const mid = findMiddles(seven.seg, four.seg);
  const top = findTop(one.seg, seven.seg);
  console.log('Before:', digits.map(d => d.digit))
  const zero = digits.find(d => d.pos.includes(0) && !d.seg.split('').includes(mid));
  if (zero) {
    zero.digit = 0;
  }
  console.log('after z:', digits.map(d => d.digit))
  const nine = digits.find(d => d.seg.length === 6 && d.pos.length === 3 && segHasOtherSeg(d, one) && d.seg.split('').includes(mid));
  if (nine) {
    nine.digit = 9;
  }
  console.log('after nine:', digits.map(d => d.digit))
  const six = digits.find(d => d.pos.includes(6) && false)
  if (six) {
    six.digit = 6;
  }
  console.log('after six:', digits.map(d => d.digit))

  const three = digits.find(d => d.seg.length === 5 && segHasOtherSeg(d, one) && d.seg.split('').includes(mid));
  if (three) {
    three.digit = 3;
  } else {
    console.log('No three?');
    console.log(`${ (mid + one.seg).split('').sort().join('')}`);
    console.log(digits.filter(d=>d.seg.length === 5))
  }
  console.log('after three:', digits.map(d => d.digit))
  console.log(one.seg, seven.seg, four.seg, eight.seg, 'TOP', top, 'MID', mid, 'BR: ? ', zero)

  const br = minus(six, one);

  const five = digits.find(d => d.seg.length === 5 && !segHasOtherSeg(d, one) && d.seg.split('').includes(br))
  if (five) {
    five.digit = 5;
  }

  const two = digits.find(d => d.seg.length === 5 && !segHasOtherSeg(d, one) && !d.seg.split('').includes(br))
  if (two) {
    two.digit = 2;
  }
  console.log(digits.filter(d => d.pos.includes(0)))
  console.log(digits.filter(d => d.pos.includes(2)))
  return 42;

  console.log('Final numbers:', [zero, one, two, three, four, five, six, seven, eight, nine])
  return [zero, one, two, three, four, five, six, seven, eight, nine];
}
const matchSegs = (seg: string, seg2: string): boolean => {
  const s1 = seg.split('').sort().join('');
  const s2 = seg2.split('').sort().join('');
  return s1 === s2;
}
const calcLine = (line: string, numbers: { seg: string, pos: number[], digit: number }) => {
  const keepers = line.split('|')[1].match(segMatch);
  console.log('Keepers', keepers);
  const s = keepers
    .map(seg => numbers.find(n => matchSegs(n.seg, seg)))
    .map(d => d.digit)
     .join('');

  return parseInt(s, 10);
}


const part2 = (input: string[]): number => {
//   const r = calcLine(input[0], processLine(input[0]));
//  console.log('R', r)
  /*
  const all = input.map(line => ({
    numbers: processLine(line),
    line
  }))
  */
  // .map(({ numbers, line }) => calcLine(line, numbers));a
  // calcLine(all[1].line, all[1].numbers);
  calcLine(input[1], processLine(input[1]))
  return 3;
  for (l = 0; l < all.length; l++) {
    try {
      const res = calcLine(all[l].line, all[l].numbers);
      console.log('Got', l);
    } catch (e) {
      console.log(`Error at line ${l}:`, e);
      console.log(all[l]);
      break;
    }
  }
  return input.length;
};


const p1 = part1(testInput);
console.log(`P1: ${p1}, ${p1 === 26 ? 'correct' : 'incorrect'}`);

const x = 'gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce';

const p2 = part2(testInput);
console.log(`P2: ${p2}, ${p2 === 61229 ? 'correct' : 'incorrect'}`);
// execute(__filename, part1, part2);

