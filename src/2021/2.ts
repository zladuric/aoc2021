import { execute } from '../util';

enum Direction {
  forward = 'forward',
  up = 'up',
  down = 'down'
}

interface Movement {
  dir: Direction;
  val: number;
}

const parseCommands = (input: string[]): Movement[] => {
  return input.map(line => {
    const [dir, val] = line.split(' ');
    return {
      dir: dir as Direction,
      val: parseInt(val, 10),
    };
  });
};

const calculateForwardMovement = (movements: Movement[]): number => {
  return movements
    .filter(movement => movement.dir === Direction.forward)
    .reduce((acc, cur) => acc + cur.val, 0);
};

const calcDownMovement = (movements: Movement[]): number => {
  return movements
    .filter(movement => movement.dir !== Direction.forward)
    .map(m => m.dir === Direction.down ? m.val : -m.val)
    .reduce((acc, cur) => cur + acc, 0);
};


const part1 = (input: string[]): number => {
  const movements = parseCommands(input);
  const forwardCount = calculateForwardMovement(movements);
  const downCount = calcDownMovement(movements);
  return forwardCount * downCount;
};

const calculateDownWithAims = (movements: Movement[]): number => {
  let aim = 0;
  let pos = 0;
  for (let mov of movements) {
    if (mov.dir === Direction.forward) {
      pos += mov.val * aim;
    } else if (mov.dir === Direction.up) {
      aim = aim - mov.val;
    } else {
      aim = aim + mov.val;
    }
  }
  console.log('Final aim:', aim);
  return pos;
};

const part2 = (input: string[]): number => {
  const commands = parseCommands(input);
  const forwards = calculateForwardMovement(commands);
  const downs = calculateDownWithAims(commands);
  return forwards * downs;
};

execute(__filename, part1, part2);
