import { execute } from '../util';

interface BingoNumber {
  val: number;
  isMarked: boolean;
}
  
type BoardNumbers = BingoNumber[][];

interface Board {
  boardNumbers: BoardNumbers;
  won: boolean;
  turnWon?: number;
}


const parseBoard = (input: string): BoardNumbers => input
	.split('\n')
	.map(line => line
		.split(/\s/)
		.map(n => parseInt(n, 10))
		.filter(v => !isNaN(v))
		.map(val => ({ val, isMarked: false }))
	)
	.filter(l => l.length);

const getBoard = (input: string): Board => ({
  boardNumbers: parseBoard(input),
  won: false,
});

const getBingoNumber = (val: string): BingoNumber => ({ val: parseInt(val, 10), isMarked: false });
const getNumberStream = (input: string) => input.split(',').map(n => parseInt(n, 10));

const hasWinningRow = (lines: BoardNumbers[][]): boolean => lines
  .some(line => line.every(num => num.isMarked))

const transpose = m => m[0].map((x,i) => m.map(x => x[i]))

const hasWinningColumn = (lines: BoardNumbers[][]): boolean => hasWinningRow(transpose(lines));


const markBoard = (board: Board, next: number, turn: number): Board => {
  if (board.won) {
    return board;
  }
  const boardNumbers = board.boardNumbers.map(line => line.map(num => {
    if (num.val === next) {
      num.isMarked = true;
    }
    return num;
  }));
  try {
  won = hasWinningRow(board.boardNumbers) || hasWinningColumn(board.boardNumbers);
  } catch (E) {
	  console.log('CAUGHT', E, board);
	  printBoard(board)
	  throw E;
  }
  return {
    boardNumbers,
    won,
    turnWon: won ? turn : undefined,
  };
}
const getLineScore = (line: BoardNumbers[]): number => line.reduce((score, n) => n.isMarked ? score : score + n.val, 0);

const printBoard = (board: Board): void => console.log(
  board.boardNumbers
    .map(line => line
      .map(num => num.isMarked ? ` ${num.val}` : `-${num.val}`)
      .join(' ')
    )
    .join('\n')
);

const calcWinningScore = (board: Board, num: number): number => {
  const sum = board.boardNumbers
    .reduce((sum, line) => sum + getLineScore(line), 0);
//   console.log('L:', board.boardNumbers.map(line => getLineScore(line)));
  printBoard(board);
//   console.log('SN', sum, num)
  return sum * num;
}
const part1 = (input: string[]): number => {
  const numbers = getNumberStream(input.slice(0, 1)[0]);
  let boards = input.slice(1).map(getBoard);
  let win = false;
  let next;
  const draw = [...numbers];
  const drawn = []
  while (!win) {
    next = draw.shift();
    drawn.push(next)
    boards = boards.map(board => markBoard(board, next))
    win = boards.some(board => board.won === true);
  }
  const winningBoard = boards.find(b => b.won)
  console.log(`Numbers drawn: ${drawn.length}/${numbers.length}. Drawn:`, drawn)
  console.log(`Boards: ${boards.length}, won: ${win}`, winningBoard);
  return calcWinningScore(winningBoard, next);
};

const sortBoardByTurnWon = (b1: Board, b2: Board) => b2.turnWon - b1.turnWon;

const part2 = (input: string[]): number => {
  const numbers = getNumberStream(input.slice(0, 1)[0]);
  let boards = input.slice(1).map(getBoard);
  let allWon= false;
  let next;
  const draw = [...numbers];
  const drawn = [];
  let turn = 0;
  while (!allWon) {
    next = draw.shift();
    drawn.push(next)
    turn++;
    boards = boards.map(board => markBoard(board, next, turn));
    allWon = boards.every(board => board.won === true);
  }
  const winningBoard = boards.sort(sortBoardByTurnWon)[0];
  console.log(`Numbers drawn: ${drawn.length}/${numbers.length}. Drawn:`, drawn, boards.filter(b => b.won).length)
  console.log(`Boards: ${boards.length}`, winningBoard);
  console.log(boards.sort(sortBoardByTurnWon).map(b => b.turnWon))
  return calcWinningScore(winningBoard, next);
};

const testInput = `7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
8  2 23  4 24
21  9 14 16  7
6 10  3 18  5
1 12 20 15 19

3 15  0  2 22
9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
2  0 12  3  7`.split('\n\n')
// console.log('TESTINPUT:', part1(testInput));
// console.log(`Part 2: ${part2(testInput)}, ${part2(testInput) === 1924 ?'correct' : 'incorrect'}.`);
execute(__filename, part1, part2, '\n\n');

