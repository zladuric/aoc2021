import { execute } from '../util';

const testInput = `3,4,3,1,2`.split('\n');

type Fish = number;
const getFish = (input: string): Fish[] => input.split(',').map(n => parseInt(n, 10));

const getSchool = (s: Fish[]): Fish[] => s.map(f => f === 0 ? [6, 8] : [f - 1]).flat();

const iterate = (school: Fish[], iteration: number): number => {
  if (iteration === 0) {
    return school.length;
  }
  const newSchool = getSchool(school);
//   console.log(`Iter: ${iteration}, school: ${school.length} -> ${newSchool.length}.`);
  return iterate(newSchool, iteration - 1);
}
const part1 = (input: string[]): number => {
  const fish = getFish(input[0]);
  console.log('Fish', fish);
  const result = iterate(fish, 80);
  console.log(`Result: ${result}`);
  return result;
};

interface Fishes {
  0: number;
  1: number;
  2: number;
  3: number;
  4: number;
  5: number;
  6: number;
  7: number;
  8: number;
}
const emptySchool: Fishes = {
  0: 0,
  1: 0,
  2: 0,
  3: 0,
  4: 0,
  5: 0,
  6: 0,
  7: 0,
  8: 0,
};



const getFishes = (input: string): Fishes => input
  .split(',')
  .reduce((fishes, f) => ({
    ...fishes,
    [f]: fishes[f] + 1,
  }), emptySchool);

const count = (fish: Fishes): number => Object.values(fish).reduce((acc, cur) => acc + cur, 0);

const addSchools = (s1: Fishes, s2: Fishes): Fishes => {
  return new Array(9).fill(0).map((_, idx) => idx)
    .map(idx => ({ [idx]: (s1[idx] || 0) + (s2[idx] || 0) }))
    .reduce((s, f) => ({ ...s, ...f }));
};

const sleepOver = (fish: Fishes, iteration: number): Fishes => {
  if (iteration === 0) {
    return count(fish);
  }
  const school = Object.entries(fish)
    .map(([key, value]) => {
      if (key > 0) {
	return { [key - 1]: value };
      } else {
	return {
	  6: value,
	  8: value,
	}
      }
    })
    .reduce((school, fishes) => addSchools(school, fishes), emptySchool);
  return sleepOver(school, iteration - 1);
};

const part2 = (input: string[]): number => {
  const fish = getFishes(input[0]);
  console.log('Fish', fish);
  const result = sleepOver(fish, 256);
  console.log(`Result: ${result}`);
  return result;
};


// execute(__filename, part1, part2);

// const p1 = part1(testInput);
const p2 = part2(testInput);
// console.log(`Part 1: ${p1}, ${p1 === 5934 ? 'correct' : 'incorrect' }.`);
console.log(`Part 2: ${p2}, ${p2 === 26984457539 ? 'correct' : 'incorrect' }.`);
// 26984457539
execute(__filename, part1, part2);
