import { execute } from '../util';

const countIncreasingDepths = (depths: number[]): [number, number] => {
  let prev = [depths.shift()];
  let current = [depths[0], depths[1], depths[2]];
  let ups = 0;
  let total = 0;
  while (current[2]) {
    total++;
    if (current.reduce((x, y) => x + y, 0) > prev.reduce((x, y) => x + y, 0)) {
      ups++;
    }
    prev = current;
    depths.shift();
    current = [depths[0], depths[1], depths[2]];
  }
  return [ups, total];
};

const countIncreasingRollingDepths = (depths: number[]): [number, number] => {
  let prev = [depths.shift()];
  let current = [depths[0], depths[1], depths[2]];
  let ups = 0;
  let total = 0;
  while (current[2]) {
    total++;
    if (current.reduce((x, y) => x + y, 0) > prev.reduce((x, y) => x + y, 0)) {
      ups++;
    }
    prev = current;
    depths.shift();
    current = [depths[0], depths[1], depths[2]];
  }
  return [ups, total];
};

const part1 = (input: string[]): number => {
  const depths = input.map(line => parseInt(line, 10));
  const [ups, totals] = countIncreasingDepths(depths);
  console.group(`Part 1: ${ ups } increases out of ${ totals } total sonar measurements.`);
  console.groupEnd();
  return ups;
};

const part2 = (input: string[]): number => {
  const depths = input.map(line => parseInt(line, 10));
  const [ups, totals] = countIncreasingRollingDepths(depths);
  console.log(`Part 2: ${ ups } increases out of ${ totals } total sonar measurements.`);
  return ups;
};

execute(__filename, part1, part2);
