import { execute } from '../util';

const MAX_X = 10000;

const testInput = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`.split('\n');

type VentMap = number[][];
type Dot = [number, number];
type Coord = {
  x: number,
  y: number
}
type Coords = {
  start: Coord,
  end: Coord,
}

const re = /(\d+),(\d+) -> (\d+),(\d+)/
const parseCoords = (line: string): Coords => {
  const numbers = line.match(re);
  if (!numbers) {
    return null;
  }
  return {
    start: {
      x: parseInt(numbers[1], 10),
      y: parseInt(numbers[2], 10),
    },
    end: {
      x: parseInt(numbers[3], 10),
      y: parseInt(numbers[4], 10),
    }
  };
}

interface Line {
  type: 'row' | 'col';
  main: number;
  fields: number[];
}

const isHorizontal = (line: Coords): boolean => line.start.y === line.end.y;
const isVertical = (line: Coords): boolean => line.start.x === line.end.x;

const isNotDiagonal= (line: Coords): boolean => isHorizontal(line) || isVertical(line);

const expandHorizontalLine = (line: Coords): Dot[] => {
  const coords = [];
  const y = line.start.y;
  const s = Math.min(line.start.x, line.end.x);
  const e = Math.max(line.start.x, line.end.x);
  for (let x = s; x <= e; x++) {
    coords.push([x, y]);
  }
  return coords;
}
const expandVerticalLine = (line: Coords): Dot[] => {
  const coords = [];
  const x = line.start.x;
  const s = Math.min(line.start.y, line.end.y);
  const e = Math.max(line.start.y, line.end.y);
  for (let y = s; y <= e; y++) {
    coords.push([x, y]);
  }
  return coords;
 
}

const expandDiagonalLine = (line: Coords): Dot[] => {
  const coords = [];
  const sx = Math.min(line.start.x, line.end.x);
  const xInc = sx === line.start.x ? 1 : -1;
  let sy = Math.min(line.start.y, line.end.y);
  const yInc = sy === line.start.y ? 1 : -1;
  let done = false;
  let x = line.start.x;
  let y = line.start.y;
  while (!done) {
   coords.push([x, y]);
   done = (x === line.end.x && y === line.end.y);

    x = x + xInc;
    y = y + yInc;
  }
  console.log(`Expanding diagonal from  (${line.start.x}, ${line.start.y}) to (${line.end.x}, ${line.end.y}):`, line, coords)
  return coords; 
}

const createVentMap = (lines: Coords[]): VentMap =>{
  const map = new Array(MAX_X).fill(null);
  lines.forEach(line => {
    let coords;
    if (isHorizontal(line)) {
      coords = expandHorizontalLine(line);
    } else if (isVertical(line)) {
      coords = expandVerticalLine(line);	
    } else {
      coords = expandDiagonalLine(line);
    }
    coords.forEach(([x, y]) => {
      if (!map[x]) {
	map[x] = new Array(MAX_X).fill('.');
      }
      if (map[x][y] === '.') {
	map[x][y] = 1;
      } else {
	map[x][y] = map[x][y] + 1;
      }
    });
  })
  return map;
}
const transpose = m => m[0].map((x,i) => m.map(x => x[i]))

const printMap = (map: VentMap): void => {
  transpose(map).forEach(row => {
    if (!row) return console.log(new Array(MAX_X).fill('.').join(''))
    console.log(row.join(''))
  });
}

const countGoodIntersections = (map: VentMap): number => map
  .reduce((acc, row) => !row ? acc : acc + row.filter(v => v >= 2).length, 0);

const part1 = (input: string[]): number => {
  const lines = input
    .map(parseCoords)
    .filter(v => v)
    .filter(isNotDiagonal)
  console.log(lines)
  const map: VentMap = createVentMap(lines);
//   printMap(map)
  console.log('Goodies:', countGoodIntersections(map))
    
  return countGoodIntersections(map);
};

const part2 = (input: string[]): number => {
  const lines = input
    .map(parseCoords)
    .filter(v => v)
  console.log(lines)
  const map: VentMap = createVentMap(lines);
//   printMap(map)
  console.log('Goodies:', countGoodIntersections(map))
    
  return countGoodIntersections(map);
};

// execute(__filename, part1, part2);

const p1 = part1(testInput);
console.log(`Part 1: ${p1}, ${p1 === 5 ? 'correct' : 'incorrect'}.`);
const p2 = part2(testInput);
console.log(`Part 2: ${p2}, ${p2 === 12 ? 'correct' : 'incorrect'}.`);


execute(__filename, part1, part2);

