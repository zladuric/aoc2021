import { readFileSync } from 'fs';
import { basename, sep } from 'path';

export const getEntireInputFile = (source: DayAndYear): string => {
  const { day, year } = source;
  const fileName = `data/inputs/${year}/${day}/input`;
  try {
    return readFileSync(fileName).toString();
  } catch (e) {
    console.error(`Cannot open ${fileName}:`, e);
  }
}

export const getInputLines = (source: DayAndYear, lineSep: string): string[] => {
  const { day, year } = source;
  const fileName = `data/inputs/${year}/${day}/input`;
  try {
    const file = readFileSync(fileName).toString();
    if (!lineSep) {
	return file;
    } else {
      return file
        .split(lineSep)
        .filter(v => v);
    }
  } catch (e) {
    console.error(`Cannot open ${fileName}`, e);
    throw e;
  }
}
interface DayAndYear {
  day: number;
  year: number;
}

export const getCurrentDayAndYear = (filename: string): DayAndYear => {

  const currentDay = basename(__filename).split('.')[0];
  const parts = __filename.split(sep);
  const currentYear = parts[parts.length - 2];
  return {
    day: parseInt(currentDay, 10),
    year: parseInt(currentYear, 10),
  }
}
export function execute(fileName: string, part1: Function, part2: Function, lineSep = '\n') {
  console.time('Execution');
  const date = getCurrentDayAndYear(fileName);
  const { day, year } = date;


  console.log(`\nAOC ${year} Day ${day}.`);
  const input = getInputLines({ day, year }, lineSep);

  console.log(`Part 1 result: ${part1(input)}.`);
  console.log(`Part 2 result: ${part2(input)}.`);

  console.timeEnd('Execution');
  console.log(`\nExecuted Day ${day} of ${year}.\n`);
}

