import { execute, getEntireInputFile } from '../util';

const testInput = `ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in`;

interface Passport {
  ecl?: string;
  hcl?: string;
  hgt?: string;
  byr?: number;
  eyr?: number;
  iyr?: number;
  pid?: number;
  cid?: number;
}

// util shit

const getNextPassport = (input: string): Passport => {
  const fields = input.split(/\s+/);
  const kv = fields
        .map(f => f.split(':'))
  console.log(kv)
  const passport = kv
        .reduce((ppt, field) => {
          const [key, val] = field;
          ppt[key] = val;
          return ppt;
        }, {});
  console.log('Pspt', passport)
  return passport;
}
// util
const isBetween = (num: string, min: number, max: number): boolean => parseInt(num, 10) >= min && parseInt(num, 10) <= max;

// individual field validators
const isByrValid = (yr: string): boolean => isBetween(yr, 1920, 2002);
const isIyrValid = (yr: string): boolean => isBetween(yr, 2010, 2020);
const isEyrValid = (yr: string): boolean => isBetween(yr, 2020, 2030);
const isHgtValid = (hgt: string): boolean => (hgt.slice(-2) === 'cm' && isBetween(hgt, 150, 193)) ||
	(hgt.slice(-2) === 'in' && isBetween(hgt, 59, 76));
const isHclValid = (hcl: string): boolean => /^#[1-9a-f]{6}$/.test(hcl);
const isEclValid = (ecl: string): boolean => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(ecl)
const isPidValid = (pid: string): boolean => /^\d{9}$/.test(pid);

// passport validator methods
const hasAllFields = (pass: Passport): boolean => !!(pass.ecl && pass.hcl && pass.byr && pass.eyr && pass.iyr && pass.pid && pass.hgt)
const isPassportReallyValid = (pass: Passport): boolean =>
  isByrValid(pass.byr) &&
  isIyrValid(pass.iyr) &&
  isEyrValid(pass.eyr) &&
  isHgtValid(pass.hgt) &&
  isHclValid(pass.hcl) &&
  isEclValid(pass.ecl) &&
  isPidValid(pass.pid);

const part1 = (_: string[]): number => {
  const contents = getEntireInputFile({ day: 4, year: 2020 });
//   const contents = testInput;
  const passwordLines = contents.split('\n\n');
  const passports = passwordLines
    .map(getNextPassport);
  return passports.map(hasAllFields).filter(v => v).length;
};


const part2 = (_: string[]): number => {
  const contents = getEntireInputFile({ day: 4, year: 2020 });
  const passwordLines = contents.split('\n\n');
  const passports = passwordLines
    .map(getNextPassport)
    .filter(hasAllFields);
  return passports.map(isPassportReallyValid).filter(v => v).length;
};


// execute(__filename, part1, part2);
// console.log(part2(testInput));

const invalids = `eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007`;

const valids = `pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719`;
// const ps = invalids.split('\n\n');
// const pass = ps.map(getNextPassport);
// console.log(pass.map(isPassportReallyValid))
/*
 * const psv = valids.split('\n\n');
const passv = psv.map(getNextPassport);
console.log(isPassportReallyValid(passv[0]))
console.log(passv.map(isPassportReallyValid));
*/
const passes = invalids.split('\n\n').map(getNextPassport);
const validx = passes.map(isPassportReallyValid);
console.log(validx.filter(v => v).length)


// execute(__filename, part1, part2)

