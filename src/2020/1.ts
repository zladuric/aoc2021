import { execute } from '../util';

/**
 * Finds two numbers adding up to 2020
 */
const findTwoNumbers = (numbers: number[], sum = 2020, skipIndex = null): [number, number] => {
  for (let i = 0; i < numbers.length; i++) {
    if (i === skipIndex) {
      continue;
    }
    for (let j = 0; j < numbers.length; j++) {
      if (i === j || j === skipIndex) {
        continue;
      }
      if (numbers[i] + numbers[j] === sum) {
        return [numbers[i], numbers[j]];
      }
    }
  }
  return [];
};

/**
 * finds three such numbers
 */
const findThreeNumbers = (numbers: number[]): [number, number, number] => {
  for (let i = 0; i < numbers.length; i++) {
    let one = numbers[i];
    let [two, three] = findTwoNumbers(numbers, 2020 - one, i);
    if (one + two + three === 2020) {
      return [one, two, three];
    }
  }
};


const part1 = (input: string[]): number => {
  const numbers = input.map(val => parseInt(val, 10));
  const [one, two] = findTwoNumbers(numbers);
  return one * two;
};

const part2 = (input: string[]): number => {
  const numbers = input.map(val => parseInt(val, 10));
  const [one, two, three] = findThreeNumbers(numbers);
  return one * two * three;
};

execute(__filename, part1, part2);


