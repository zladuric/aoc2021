import { execute } from '../util';

interface Slope {
  right: number,
  down: number
}

const getSlopeLine = (line: string, pos: number): string => {
  let res = line;
  while (res.length <= pos) {
    res = `${ res }${ line }`;
  }
  return res;
};

const getMapAtPos = (line: string, pos: number) => line[pos];
const isTree = (place: string): boolean => place === '#';

const isOnSlope = (slope: Slope, idx: number): boolean => idx % slope.down === 0;

const countTreesForSlope = (map: string[], slope: Slope): number => {

  return map
    .map((line, idx) => getSlopeLine(line, idx * slope.right))
    .filter((line, idx) => isOnSlope(slope, idx))
    .map((line, idx) => getMapAtPos(line, idx * slope.right))
    .map(place => isTree(place))
    .filter(v => v)
    .reduce(acc => acc + 1, 0);
};


const part1 = (input: string[]): number => {
  const slope = {
    right: 3,
    down: 1,
  };
  return countTreesForSlope(input, slope);
};

const part2 = (input: string[]): number => {
  const slopes: Slope[] = [
    { right: 1, down: 1 },
    { right: 3, down: 1 },
    { right: 5, down: 1 },
    { right: 7, down: 1 },
    { right: 1, down: 2 },
  ];
  return slopes
    .map(slope => countTreesForSlope(input, slope))
    .reduce((acc, cur) => acc * cur, 1);
};

execute(__filename, part1, part2);

const testInput = `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`.split('\n');

console.log('Test Lines:', testInput.length);

console.log('P1 test', part1((testInput)));
console.log('P2 test ', part2(testInput));

console.log('Last slope:', countTreesForSlope(testInput, { right: 1, down: 2 }));
