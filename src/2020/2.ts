import { execute } from '../util';

interface PassRule {
  letter: string;
  min: number;
  max: number;
}

const getRule = (str: string): PassRule => {
  // "1-3 z"
  const pattern = /(\d+)-(\d+) ([a-zA-Z])/;
//   console.log(`Pattern match for ${str}:`, str.match(pattern));
  const [_, min, max, letter] = str.match(pattern);
  return {
    min: parseInt(min, 10),
    max: parseInt(max, 10),
    letter,
  };
};

const getRuleAndPass = (line: string): [PassRule, string] => {
  const [ruleStr, pass] = line.split(':').map(val => val.trim());
  const rule = getRule(ruleStr);
  return [rule, pass];
};


const validateLetterCount = (line: string): boolean => {
  const [rule, pass] = getRuleAndPass(line);
  const count = pass.match(new RegExp(rule.letter, 'g'))?.length;
  return count >= rule.min && count <= rule.max;
};

const validateLetterPos = (line: string): boolean => {
  const [rule, pass] = getRuleAndPass(line);
  /*
  const minRe = new RegExp(`^.{${rule.min}}[${rule.letter}]`);
  const maxRe = new RegExp(`^.{${rule.max}}[${rule.letter}]`);
  console.log('MinRe:', minRe, pass.match(minRe), 'MaxRe:', maxRe, pass.match(maxRe));
  console.log('Pass one or the other:', minRe.test(pass) !== maxRe.test(pass));
  */
  return (pass[rule.min - 1] === rule.letter) !== (pass[rule.max - 1] === rule.letter);
//  return minRe.test(pass) !== maxRe.test(pass);
};


const part1 = (input: string[]): number => {
  return input.map(line => validateLetterCount(line)).filter(v => v).length;
};

const part2 = (input: string[]): number => {
  return input.map(line => validateLetterPos(line)).filter(v => v).length;
};
execute(__filename, part1, part2);
