const glob = require('glob');

const years = ['2019', '2020', '2021'];
const files = years.map(y => `src/${y}/*.ts`);
const entryPoints = files.map(f => glob.sync(f)).flat();
console.log('Watching :', entryPoints);
require('esbuild').build({
  entryPoints,
  bundle: true,
  outdir: 'dist',
  watch: true,
  platform: 'node',
  sourcemap: true,
  color: true,
  minify: false,
  logLevel: 'info',
}).catch(() => process.exit(1))

