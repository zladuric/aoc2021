const nodemon = require('nodemon');

let day = new Date().getDate();
let year = new Date().getFullYear();

if (process.argv[2]) {
  day = parseInt(process.argv[2], 10);
}
if (process.argv[3]) {
  year = parseInt(process.argv[3], 10);
}

if (isNaN(day) || day < 1 || day > 25) {
  throw new Error('Day must be a number between 1 and 25');
}

if (isNaN(year) || year < 2015 || year > new Date().getFullYear()) {
  throw new Error('Year must be between 2015 and now.');
}

const filename = `dist/${year}/${day}.js`;

nodemon({
  script: filename
});
nodemon.on('start', function () {
  console.log('App has started');
}).on('quit', function () {
  console.log('App has quit');
  process.exit();
}).on('restart', function (files) {
  console.log('App restarted due to: ', files);
});
