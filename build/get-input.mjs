import fetch from 'node-fetch';
import { existsSync, writeFileSync } from 'fs';
import mkdirp from 'mkdirp';
import { config } from 'dotenv';

// get env.
config();
const sessionCookie = `session=${process.env.AOC_SESSION_COOKIE}`;

const delay = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve(), 400);
  })
}

const getInputUrl = (year, day) => {
  return `https://adventofcode.com/${year}/day/${day}/input`
}
const getFileName = (year, day) => `data/inputs/${year}/${day}/input`

const saveFile = (filename, content, year, day) => {
  console.log('Creating file for', year, day)
  mkdirp.sync(`data/inputs/${year}/${day}`);
  writeFileSync(`data/inputs/${year}/${day}/input`, content);
  console.log('Created.')
}

const isDownloaded = filename => existsSync(filename);

const getInput = (url) => {
  console.log('Fetching input')
  return fetch(url, {
    headers: {
      'Cookie': sessionCookie,
      'User-Agent': 'node',
    }
  })
    .then(res => res.text());
}


const getAllDays = async () => {
  const years = [2015, 2016, 2017, 2018, 2019, 2020, 2021];
  const days = new Array(25).fill('').map((_, i) => i + 1);
  for (let year of years) {
    for (let day of days) {
      const filename = getFileName(year);
      if (!isDownloaded(filename)) {
        try {
          const text = await getInput(getInputUrl(year, day))
          saveFile(filename, text, year, day);
          await delay();
        } catch (e) {
          console.log('FAILED getting', e, 'for', year, day);
        }

      }
    }
  }
}

const getDataFor = async (day, year) => {
  try {
    const text = await getInput(getInputUrl(year, day))
    const filename = getFileName(year, day)
    saveFile(filename, text, year, day);
  } catch (e) {
    console.error(e);
  }

}

/*
getInput(getInputUrl(2015, 1))
  .then(res => {
    console.log('Got response', res);
    saveFile(getFileName(2015, 1), res);
  })
  .catch(e => console.error(e));

*/

const what = process.argv[2];
if (what === 'all') {
  console.log(getAllDays());
} else if (!what) {
  usage()
} else if (what === 'today') {
  const year = new Date().getFullYear();
  const day = new Date().getDate();
  if (isNaN(day) || day > 25 || day < 1) {
    usage('Day must be between 1 and 25.')
    process.exit(1);
  }
  if (isNaN(year) || year < 2015 || year > new Date().getFullYear()) {
    usage('Year must be between 2015 and now.');
    process.exit(1);
  }
  getDataFor(day, year);
} else {
  const day = parseInt(process.argv[2], 10);
  console.log(`Getting input for ${day}`);
  if (isNaN(day) || day > 25 || day < 1) {
    usage('Day must be between 1 and 25.')
    process.exit(1);
  }
  const year = process.argv[3] ? parseInt(process.argv[3], 10) : new Date().getFullYear();
  if (isNaN(year) || year < 2015 || year > new Date().getFullYear()) {
    usage('Year must be between 2015 and now.');
    process.exit(1);
  }
  getDataFor(day, year);
  console.log('Data downloaded.');
}



function usage(why) {
  if (why) {
    console.log(`Error: ${why}
`);
  }
  console.log(`Usage:

node get-input <all | today | day [year] >
   
  `)
}
